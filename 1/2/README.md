# 1.2 - Multicontainer-App mit "docker-compose"
In dieser Übung erstellst Du eine einfache **Multicontainer-Applikation** - also einen Dienst, der aus **mehr als einem Container** besteht. Du nutzt dazu erstmals **IaC** (Infrastructure as Code); also ein **deklaratives  Manifest**, welches definiert, **wie** diese sogenannten **Microservices** zusammenarbeiten. Z.B. über **welche Ports** sie verfügbar gemacht werden und **wo** die Daten persistiert werden. Der entsprechende Code wird in einem **.yaml**- oder **.json** abgelegt. Bei dieser Übung heisst dieses IaC-Manifest **docker-compose.yaml**.

Damit verabschieden wir uns jetzt auch langsam aber sehr sicher von der sogenannten **Monolithischen Architektur**, welche bisher auch in der Enterprise-Welt standardmässig eingesetzt wurde.  

**Monolithen** können nachteilhaft sein, da sie oft schwerfällig sind. Änderungen an einem Teil des Systems können zu unerwarteten Auswirkungen auf andere Teile führen. Zudem kann es schwierig sein, neue Technologien oder Frameworks zu integrieren, da dies oft das gesamte System betreffen kann.

Das Aufteilen eines Monolithen in **verschiedene kleinere Microservices** ergibt demzufolge gleich **mehrere** Vorteile. Hier die **vier** wichtigsten:

- Höhere Skalierbarkeit
- Bessere Wartbarkeit
- Einfachere Bereitstellung
- Größere Flexibilität bei der Entwicklung und Integration von Funktionen. 

**Fazit :**
Durch die Aufteilung von Anwendungen in **kleinere, unabhängig voneinander entwickelbare Dienste** können Teams effizienter arbeiten und Änderungen schneller implementieren, ohne das Gesamtsystem zu beeinträchtigen.

## Aufgabenstellung 1.2

#### Ablauf:

**1.** Im Tutorial (knapp 8') lernst Du nach einem kurzen Intro die Anwendung von **docker-compose** kennen. Hier werden die **Vorteile** einer deklarativen Anwendung gut ersichtlich. Es braucht grundsätzlich nur noch ein Kommando, um die Applikation zu starten oder zu stoppen.  
**2.** Im zweiten Schritt wirst Du dieselbe Anwendung noch ein bisschen **customizen** erstellen und in **Deinem Gitlab-Registry** ablegen. Bei späteren Übungen wirst Du dieses Image wieder benötigen.


### 1. Tutorial "docker-compose" anschauen

#### Vorgehen:
- Tutorial **in Ruhe anschauen** - Kopfhörer empfohlen, Selbststudium  
- [**Text-File**](./02_Journal-Docker-Bootstrap.txt) welches im Tutorial benutzt wird
- **Wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. docker-compose Commands). Diese können nachher beim 2. Teil (Hands-on Übungen) nützlich sein.


![Video1:](../../images/Video.png) 07:41
[![Tutorial](../../images/docker-part2_200.png)](https://web.microsoftstream.com/video/c376d6ef-7711-458e-bccd-847abe95468c?list=studio)


<br>

------

<br>
 
### 2. Questions and Answers

Bevor Du später im 3. Teil die **bestehenden Config-Files** im Repo gmäss **Vorgabe** ändern wirst, hier nochmals **wichtige Fragen** zu Begriffen, **die im Tutorial ausführlich erklärt werden** und die Du in Deinem Repository nochmals zusammenfassen sollst.


#### Check: Topics, die in der Doku festgehalten sein sollten:

- [x] Erkläre den Unterschied zwischen **Imperativ** und **Deklarativ**
- [x] Nenne **3 Vorteile** der **deklarativen** Anwendung
- [x] Was bedeutet die Abkürzung **IaC** ausgeschrieben
- [x] Nenne und begründe **3 Vorteile** von **IaC**
- [x] Mit welchem Protokoll arbeitet die **REST-API Schnittstelle** ? 

#### Weitere formative Fragen, die sich auf das Tutorial und den folgenden Auftrag beziehen:
- [ ] Wieviele Container werden für die Anwendung gebraucht?
- [ ] Wie arbeiten diese Container zusammen?
- [ ] Was ist ein **Redis-Cache**? Einfache Erklärung reicht
- [ ] In welchem File sind die **Dependencies** (oder Abhängigkeiten) abgelegt?
- [ ] Welches File bestimmt, **wie** die Microservices (Container) zusammenarbeiten?
- [ ] Auf welchem Port **horcht** Redis standardmässig und **wo** wird dieser Port definiert?

<br>

------

<br>

### 3. Hands-on: docker-compose

Erstelle eine Applikation mit **docker-compose**, welche **verschiedene Bedingungen** (siehe weiter unten) erfüllen muss. Dokumentiere alles so in Deinem Repository, dass Du die Applikation zu einem späteren Zeitpunkt einfach und schnell wieder nutzen und weiterentwickeln könntest.

#### Voraussetzungen
- Falls Du dies im letzten Auftrag noch nicht gemacht hast, solltest Du zuerst das Repository [**Docker-bootstrap**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/) klonen und dann rein"hüpfen". 
- Für diese Übung wechselst Du am besten gleich in das
Unterverzeichnis [**multi-container**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/multi-container). 



#### Bedingungen:
- **Port** auf der die App laufen soll ist neu: **5001** 
- Der **Name** des Netzwerks (Austausch zwischen Containern) heisst neu: **`Internes_Netz`** (Achte darauf, dass dieser Label nicht nur an einer Stelle geändert werden muss)<br>
- Der **Name** des Volumes heisst neu: **`Zaehler_Speicher`** (Achte darauf, dass dieser Label nicht nur an einer Stelle geändert werden muss)<br>
- Der Titel auf dem Frontend lautet nicht mehr `Klicke auf refresh, um Dein Skill-Level zu erhöhen` sondern neu:<br>
:mag_right: **`Klicke auf refresh, um Dein Knowhow zu bestätigen`** ...der Rest bleibt gleich.


#### Beachten:
- Damit die Bedingungen oben allesamt erfüllt sind,  **musst** Du in diversen Files des geklonten Repos Änderungen vornehmen. Du weisst ja inzwischen, wie das geht :repeat:

<br>

----

<br>

#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Beweisen, dass **vor** dem Start der Vorführung **kein** Dienst läuft:  `<docker container ls -a | grep -i multicontainer`
- [ ] Beweisen, dass nach der Eingabe von `$ docker-compose up -d` über **Port 5001** auf die Applikation zugegriffen werden kann: `<IP-Deiner-TBZ-VM:5001`
- [ ] Das Interne Netzwerk neu `Internes_Netz` heisst
- [ ] Das Volume neu `Zaehler_Speicher` heisst
- [ ] Der Titel auf dem Frontend neu mit folgendem Satz beginnt: `Klicke auf refresh, um Dein Knowhow zu bestätigen` <br>

Beweise, dass **sämtliche** Bedingungen erfüllt sind        


<br>

---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (OCI-Images & Container administrieren mit Docker)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---