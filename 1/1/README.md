# Docker Image aufsetzen, in Registry ablegen und deployen

## Aufgabenstellung 1.1

#### Ablauf:

**1.** Zuerst wird anhand eines Tutorials gezeigt, wie man Docker Images erstellt, in einem Registry ablegt und von da aus deployed.
**2.** Im zweiten Schritt wirst Du selber ein OCI-Image erstellen und in **Deinem Gitlab-Registry** ablegen. Bei späteren Übungen wirst Du dieses Image wieder benötigen.


### 1. Tutorial anschauen: Docker Image und Container aufsetzen

#### Vorgehen:
- Tutorial **in Ruhe anschauen** - Kopfhörer empfohlen, Selbststudium  
- [**Text-File**](./01_Journal-Docker-Bootstrap.txt) welches im Tutorial benutzt wird
- **Wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. Docker-Commands). Diese können nachher beim 2. Teil (Hands-on Übungen) nützlich sein.


![Video1:](../../images/Video.png) 14:07
[![Tutorial](../../images/docker-part1_200.png)](https://web.microsoftstream.com/video/b4143f73-2f96-446d-ba66-c44e8ddcb270)


<br>

------

<br>

### 2. Hands-on: Eigenes OCI-Images erstellen und im persönlichen Repository ablegen 

Erstelle **ein personifiziertes OCI-Image** mit Docker, welches **verschiedene Bedingungen** (siehe weiter unten) erfüllen muss. Lege dieses Image später auf Gitlab in **Deinem Repository** ab und dokumentiere alles so, damit Du zu einem späteren Zeitpunkt noch weisst, wie Du darauf zugreifen kannst.

#### Voraussetzungen
- Du musst das Repository [**Docker-bootstrap**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/) klonen, um später die darinliegenden Files gemäss den Bedingungen weiter unten anzupassen.
- Für diese Übung benötigst Du die Daten aus dem
[Unterverzeichnis **first-container**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/first-container) des obigen Repositories.



#### Bedingungen:
- **Port** auf der die App laufen soll: **8091** 
- **Name** des OCI-Images: **`registry/<deingitlabname>/repository/webapp_<deingitlabname>_<Port-Nr.>:1.0`** (Ergänze hier Deinen Usernamen und den Port auf dem Deine App horcht)<br>
:mag_right: Beispiel: `registry.gitlab.com/ser-cal/m346-services/webapp_ser-cal_8091:1.0`
- **Hintergrundfarbe** des Textes _"Diese Webapp läuft in einem Container"_ (bisher Lila), erhält **neu** die Farbe **Goldgelb** mit dem Hex-Wert: **#ebd63d** :mag_right: ![Goldgelb](texthintergrundfarbe_ebd63d.png)
- **Ersetze** das **alte** image.png (Titel **Cloud Native**) mit dem **neuen** image.png (Titel **Modul 346**). Verwende dazu [**DIESES File**](./image.png) (_image.png_)
    <table><tr>
    <td> <img src="image-old.png" alt="Drawing" style="width: 200px;"/> </td>
    <td> <img src="image.png" alt="Drawing" style="width: 200px;"/> </td>
    </tr></table>


#### Beachten:
- Bitte bedenke, dass Du **nicht einfach** Daten kopieren und einfügen kannst. Z.B. **IP-Adressen**, **URL's**, **Benutzernamen** etc... Passe die Files so an, dass sie auf **Deiner Umgebung** funktionieren.
- Damit die Bedingungen oben allesamt erfüllt sind,  **musst** Du in diversen Files des geklonten Repos Änderungen vornehmen. Als kleiner **Mini-Spoiler** sind hier drei aufgelistet:
    - **main.css**
    - **image.png**
    - **app.js**

<br>

----

<br>

#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Beweisen, dass **KEIN** OCI-Image auf der TBZ-VM liegt `$ docker image ls`
- [ ] **Personifiziertes Container-Image** mit `$ docker run ...` auf Deiner TBZ-VM starten (OCI-Image muss vom Gitlab-Repo ge"downloaded" werden)
- [ ] Via Webbrowser vom eigenen Laptop darauf zugreifen `IP-Adresse:Port`
- [ ] Container stoppen und dann Image löschen (Funktioniert das? Begründung): `$ docker container <ID> stop` / `$ docker rmi -f <ID>`

Beweise, dass **sämtliche** Bedingungen erfüllt sind        
- [ ] **Port: 8091**  
- [ ] **Bild:** Cloud-native ersetzt mit **Modul 346**
- [ ] **Texthintergrund**: Lila ersetzt mit **Goldgelb**

<br>




---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (OCI-Images & Container administrieren mit Docker)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---