# 1.3 - Container Orchestrierung mit Docker Swarm (Imperative Methode)

#### Einstieg

Container-Orchestrierung ist ein Prozess, bei dem Container-Anwendungen auf **mehreren Hosts** verwaltet und koordiniert werden, um die **Verfügbarkeit, Skalierbarkeit** und **Zuverlässigkeit** dieser Anwendungen zu verbessern. Die Orchestrierung ermöglicht es also, Applikationen in einer **verteilten Umgebung** zu betreiben und zu skalieren, indem sie automatisiert wird.

Container-Orchestrierungsplattformen wie **Docker Swarm** und **Kubernetes** bieten Funktionen wie **Service Discovery, Load Balancing, Skalierung** und **Auto-Healing**, um die Verwaltung von Container-Anwendungen zu vereinfachen und wichtige Services hochverfügbar und zeitnah bereitzustellen.

Für Einsteiger in das Thema Container-Orchestrierung ist es empfehlenswert, mit **einfacheren** Orchestrierungsplattformen wie **Docker Swarm** zu beginnen. Das vereinfacht später den Einstieg in komplexere Plattformen wie **Kubernetes**.

Zusammengefasst kann man also sagen, dass **Docker Swarm** sämtliche Bedingungen einer klassischen **Orchestrierungsplattform** erfüllt und im Vergleich zu Kubernetes **einiges einfacher zu nutzen** ist. Diese Lösung ist also ideal für Einsteiger und für Teams, die bereits mit Docker vertraut sind.

Es gibt aber auch wesentliche Nachteile. Folgende Topics zeigen die Grenzen von **Docker Swarm** im Vergleich zu **Kubernetes** auf.

- **Begrenzte Skalierbarkeit:** Docker Swarm ist nicht so gut skalierbar wie Kubernetes, insbesondere für sehr große Workloads.
- **Weniger Anpassungsmöglichkeiten:** Docker Swarm unterstützt nicht so viele Plugins und Add-Ons von Drittanbietern wie Kubernetes und bietet daher weniger Anpassungsmöglichkeiten.
- **Eingeschränktes Ökosystem:** Kubernetes hat ein umfangreicheres Ökosystem mit vielen Tools und Diensten, die nahtlos mit Kubernetes zusammenarbeiten, während Docker Swarm ein kleineres Ökosystem hat.
- **Weniger Flexibilität:** Docker Swarm ist nicht so flexibel wie Kubernetes und hat weniger Konfigurationsmöglichkeiten für fortgeschrittene Anwendungen.
- **Komplexität:** Docker Swarm ist zwar einfacher zu bedienen als Kubernetes, kann aber für größere und komplexere Anwendungen immer noch sehr komplex sein.
- **Fehlende Funktionen:** Docker Swarm bietet nicht alle Funktionen und Fähigkeiten, die Kubernetes bietet, wie beispielsweise die Möglichkeit, mehrere Cluster zu verwalten.
- **Unterstützung:** Docker Swarm wird von Docker Inc. entwickelt und unterstützt, während Kubernetes von der Cloud Native Computing Foundation (CNCF) unterstützt wird. Dies bedeutet, dass Kubernetes möglicherweise mehr Ressourcen und Support hat als Docker Swarm.

**Fazit :**
Kubernetes ist eine umfassendere Orchestrierungsplattform, die besser für große, komplexe Projekte geeignet ist und bedeutend mehr Funktionen und Flexibilität bietet als Docker Swarm. Es erfordert jedoch auch eine steilere Lernkurve und mehr Verwaltungsaufwand. 

Aus diesem Grund werden wir uns in dieser Übung zuerst einmal mit **Docker Swarm** auseinandersetzen und so ein gutes Fundament setzen für **Kubernetes** :rocket:


## Aufgabenstellung 1.3

#### Ablauf:

**1.** Im Tutorial (ca. 10') lernst Du nach einem kurzen Intro die Anwendung von **Docker Swarm** kennen. Wir werden anhand eines Beispiels sehen, wie man mit wenigen Manipulationen eine Anwendung skalieren kann.
**2.** Im zweiten Schritt wirst Du dieselbe Anwendung noch ein bisschen nach Vorgaben **customizen** und in **Deinem Gitlab-Registry** dokumentieren.


### 1. Tutorial "Docker Swarm (Part 3)" anschauen

#### Vorgehen:
- Tutorial **in Ruhe anschauen** - Kopfhörer empfohlen, Selbststudium  
- [**Text-File**](./03_Journal-Docker-Bootstrap.txt) welches im Tutorial benutzt wird
- **Wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. `docker service create...` Command). Diese können nachher beim 2. Teil (Hands-on Übungen) nützlich sein.


![Video1:](../../images/Video.png) 10:11
[![Tutorial](../../images/docker-part3_200.png)](https://web.microsoftstream.com/video/5da624e2-6a09-45f3-922b-a4ac5e9a59f6?list=studio)


<br>

------

<br>
 
### 2. Questions and Answers

Hier noch ein paar **Fragen** zu Begriffen, **die im Tutorial ausführlich erklärt werden** und die Du am besten noch in Deinem Repo verewigst.


#### Check: Topics, die in der Doku festgehalten sein sollten:

- [x] Erkläre, wie bei **Docker Swarm** die Verfügbarkeit sichergestellt wird
- [x] Welche Vorteile bietet der Begriff **Scalability**?
- [x] Welche Aufgabe übernimmt der **Scheduler** bei Docker Swarm?
- [x] Was bedeutet **Split Brain** und wie reagiert **Docker Swarm** auf eine solche Situation?
- [x] Was **muss** berücksichtigt werden, damit diese Übung durchgeführt werden kann? (Stichwort: `pull`)
<br>

------

<br>

### 3. Hands-on: Docker Swarm

Lade zuerst das früher erstellte **OCI-Image** von Gitlab auf deine Testumgebung.


#### Voraussetzungen
- Falls Du dies im letzten Auftrag noch nicht gemacht hast, solltest Du zuerst das Repository [**Docker-bootstrap**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/) klonen und dann rein"hüpfen". 
- Für diese Übung wechselst Du am besten gleich in das
Unterverzeichnis [**multi-container**](https://gitlab.com/ser-cal/docker-bootstrap/-/tree/main/multi-container). 


<br>

----

<br>

#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Beweisen, dass **vor** dem Start der Vorführung **kein** Dienst läuft:  `<docker container ls -a | grep -i webserver`
- [ ] Starte mit dem vorher heruntergeladenen OCI-Image einen Service, der `swarm-web` heisst und **5 Replicas** besitzt 
- [ ] Gebe den Befehl `docker service ps swarm-web` ein und überprüfe, ob die Bedingungen erfüllt sind
- [ ] Zeige die **ID's** der Container _`(Kommando selber recherchieren)`_
- [ ] Erstelle **3** weitere Replicas _`(Kommando selber recherchieren)`_
- [ ] Entferne **2** der alten Replicas mit `docker container rm <Container-ID1> <Container-ID2>`
- [ ] Beweise, dass der **Desired State** wieder hergestellt wurde _`(Kommando selber recherchieren)`_
- [ ] Erkläre mit Fachbegriffen, weshalb das so ist
- [ ] Clean-up: `docker service rm swarm-web`
- [ ] 1. Beweis, dass gelöscht: `docker service ls | grep -i web`
- [ ] 2. Beweis, dass gelöscht: `docker container ls | grep -i web`
 <br>

Der Auftrag ist abgeschlossen, sobald **sämtliche** Bedingungen erfüllt sind. 

<br>

---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (OCI-Images & Container administrieren mit Docker)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---