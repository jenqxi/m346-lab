Journal für das Tutorial Docker-Bootstrap 
==========================================

Dritte Übung --> 

3. Tutorial (Docker Swarm)

  Docker Swarm initialisieren und einen Service mit 3 Replicas erzeugen (später auf 10 replicas skalieren)
  ----------------------------------------------------------------------
$ docker swarm init					# wird im Tutorial erklärt
$ docker pull registry.gitlab.com/ser-cal/docker-bootstrap/webserver_one:1.0		# damit das nächste Kommando funktioniert, muss das entsprechende Image vorher downgeloaded werden
$ docker service create --name web -p 8080:8080 --replicas 3 registry.gitlab.com/ser-cal/docker-bootstrap/webserver_one:1.0

$ docker service ps web
$ docker service ls | grep -i webserver
$ docker container ls | grep -i webserver

  Container skalieren
$ docker service scale web=10				# Container skalieren (hier auf 10)
$ docker container ls | grep -i webserver		
$ docker container rm -f <1.ContainerID> <2.ContainerID> <3.ContainerID>	# Drei Container löschen und schauen, was passiert
$ docker container ls | grep -i webserver		# überprüfen und feststellen, dass wieder 10 Container laufen (desired state) - wurden wieder erzeugt

  Clean-up:
$ docker service rm web
$ docker service ls | grep -i webserver			# überprüfen, ob gelöscht
$ docker container ls | grep -i webserver		# überprüfen, ob gelöscht


