# OCI Images & Container Administration mit Docker



## Aufträge

#### 1. Auftrag
- [Theorie / Hands-on](1/README.md) - OCI-Image mit **Docker** erstellen, in Registry ablegen, deployen & Container Administration 


#### 2. Auftrag 
- [Theorie / Hands-on](2/README.md) - Multicontainer-App mit **docker-compose** deklarativ deployen und administrieren

#### 3. Auftrag 
- [Theorie / Hands-on](3/README.md) - Container Orchestrierung mit **Docker Swarm** (Imperative Methode)

#### 4. Auftrag 
- [Theorie / Hands-on](4/README.md) - Container Orchestrierung mit **Docker Stack** (Deklarative Methode)


<br>

---

> [⇧ **Zurück zur Hauptseite**](../README.md)
