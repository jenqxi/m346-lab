[10]: https://github.com
[20]: https://web.microsoftstream.com/video/663c03fd-5562-42f0-855f-fc98e7769c3e


![M346-Banner](images/01_banner.png)


---

# M346 LAB

In diesem Repository sind diverse Hands-on Challenges zu meistern. 
Diese sind wie folgt aufgebaut:
- Theorie: Diese wird Anhand eines Tutorials praxisnah vorgeführt. Es empfiehlt sich dieses gleich selber nachzubauen.
- Praxis: Du erhältst einen Arbeitsauftrag (Bezug zur Theorie oben). Führe diesen selbständig aus und okumentiere das Vorgehen.

Viel Erfolg und viel Spass.


[TOC]

<br>

## Challenges

### OCI Images & Container Administration mit Docker 
* [**Entrypoint**](1/README.md) (_Key aspect: **Docker**_)

    - ##### [1. Auftrag](1/1/README.md) - OCI-Image mit **Docker** erstellen, in Registry ablegen, deployen & Container Administration 

    - ##### [2. Auftrag](1/2/README.md) - Multicontainer-App mit **docker-compose** deklarativ deployen und administrieren

    - ##### [3. Auftrag](1/3/README.md) - Container Orchestrierung mit **Docker Swarm** (Imperative Methode)

    - ##### [4. Auftrag](1/4/README.md) - Container Orchestrierung mit Docker Stack (Deklarative Methode)


---

### Cloud-init, OCI Images & Container in der AWS Cloud
* [**Entrypoint**](2/README.md) (_Key aspect: **AWS**_)

    - ##### [1. Auftrag](2/1/README.md) - eMail-Alias & AWS Free Tier Account erstellen 

    - ##### [2. Auftrag](2/2/README.md) - AWS Alias und Billing Alarm erstellen

    - ##### [3. Auftrag](2/3/README.md) - AWS IAM User-Account einrichten (TOBEDONE)

--- 

### OCI Images & Container Administration mit Podman
* [**Entrypoint**](3/README.md) (_Key aspect: **Podman**_)

--- 

### OCI Images & Container orchestrieren mit Kubernetes
* [**Entrypoint**](4/README.md) (_Key aspect: **Kubernetes**_)


<br>

## Ressourcen

- 
- 
<br>

---

<br>

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)







<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>

- - -

- Autor: Marcello Calisto
- Mail: marcello.calisto@tbz.ch
