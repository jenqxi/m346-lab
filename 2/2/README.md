# AWS Alias und Billing Alarm erstellen

## Aufgabenstellung 2.2

Das Setzen eines Aliases und eines Billing Alarms verbessert die Verwaltung und Kontrolle des AWS Accounts. Der Alias erleichtert die Identifizierung und Zuordnung, während der Billing Alarm dabei hilft, Kosten im Blick zu behalten und vor unerwarteten Ausgaben zu schützen.

Aus folgenden Gründen ist es sinnvoll, in einem AWS Account einen **Alias** und einen **Billing Alarm** zu setzen:
- **Alias:** Ein Alias erleichtert die Identifizierung und Verwaltung des AWS Accounts, insbesondere wenn mehrere Accounts vorhanden sind. Durch die Verwendung eines Alias anstelle der numerischen Account-ID wird die Benutzerfreundlichkeit verbessert und die Möglichkeit von Fehlern bei der Verwendung des AWS Accounts reduziert.
- **Billing Alarm:** Ein Billing Alarm ermöglicht die Überwachung der Kosten und Ausgaben im AWS Account. Durch das Festlegen eines Schwellenwerts kann ein Alarm ausgelöst werden, wenn die Kosten einen bestimmten Betrag erreichen oder überschreiten. Dies hilft dabei, unerwartete Kostensteigerungen zu erkennen und rechtzeitig zu reagieren, um das Budget einzuhalten und Kosten zu kontrollieren.

----

### Achtung!
Folgende drei Schritte müssen **vor** dem Tutorial durchgeführt werden. Sonst hast Du mit einem Standard-User **keine** IAM-Berechtigungen.

Mit Root-User einloggen und im **Account** für sämtliche Benutzer, die später erstellt werden, den **IAM Access** aktivieren (siehe folgende drei Bilder):

**1. Oben rechts Account auswählen**<br>
![Account:](./images/01_account.png)

---

**2. ...runterscrollen bis zu "IAM User and Role Access to Billing Information" und "Edit" anklicken**<br>
![IAM-U+R_A1:](./images/02_IAM-User_Role_Access_to_Billing.png)

---

**3. "Activate IAM Access" anklicken**<br>
![IAM-U+R_A2:](./images/03_IAM-User_Role_Access_to_Billing.png)

<br>

----------

#### Ablauf:

- **1.** Zuerst wird anhand eines Tutorials gezeigt, wie man in einem AWS-Account einen **Alias** und einen **Billing Alarm** erstellt. 
- **2.** Im zweiten Schritt erstellst Du für **Deinen AWS-Account** einen **Alias** und einen **Billing Alarm** (der an Deine eMail-Adresse geschickt wird) 


### 1. Tutorial anschauen: AWS Aliasund Billing Alarm konfigurieren

#### Vorgehen:
- Tutorial **in Ruhe anschauen** - Kopfhörer empfohlen, Selbststudium. 
- **Wichtige und nutzvolle** Informationen im eigenem Repo dokumentieren (z.B. Alias-Namen und wo die Alarme angeschaut und angepasst werden können).


![Video1:](../../images/Video.png) 5:56
[![Tutorial](../../images/aws-billing-alarm.png)](https://web.microsoftstream.com/video/7ef8b980-b3bb-479e-9bdc-e6ac4f60a626?list=studio)


<br>

------

<br>

### 2. Hands-on: Alias und Billing Alarm für eigenen AWS-Account erstellen

Erstelle für Deinen AWS-Account einen eigenen **Alias** und einen **Billing Alarm** und dokumentiere alles so, damit Du zu einem späteren Zeitpunkt darauf zurückgreifen kannst.

#### Voraussetzungen
- Die drei Schritte, die zu Beginn erklärt werden, müssen vorher durchgeführt werden. Sonst hast Du keine Berechtigungen.

<br>

----


#### Nachweis:
Führe Deinem Coach anhand einer Live-Demo folgende Topics vor: 
- [ ] Du kannst zeigen, wo man als Root-User den IAM-Access für gewöhnliche Benutzer freischaltet (3 Schritte oben)
- [ ] Du hast einen AWS Account-Alias erstellt und kannst mit diesen einloggen (anstatt mit der 12-stelligen Account ID)
- [ ] du hast einen Billing Alarm erstellt und so eingerichtet, dass Dir dieser beim Überschreiten einer Limite ein eMail sendet. 
- [ ] Zeige Dein nachgeführtes Repo (alle wichtigen Schritte sind darin abgebildet und beschrieben)
<br>


---

> [⇧ **Zurück zum Entrypoint dieses Kapitels** (Cloud-init, OCI Images & Container in der AWS Cloud)](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ser-cal/m346-lab)

---

---