# Cloud-init, OCI Images & Container in der AWS Cloud



## Aufträge

#### 1. Auftrag
- [Theorie / Hands-on](1/README.md) - eMail-Alias & AWS Free Tier Account erstellen


#### 2. Auftrag 
- [Theorie / Hands-on](2/README.md) - AWS Alias und Billing Alarm erstellen

#### 3. Auftrag 
- [Theorie / Hands-on](3/README.md) - AWS IAM User-Account einrichten (TOBEDONE)

#### 4. Auftrag 
- [Theorie / Hands-on](4/README.md) - TBD


<br>

---

> [⇧ **Zurück zur Hauptseite**](../README.md)
